package lu.ausy.java;

public class Collatz {


    /*I must rewrite that to use a Tuple instead of a in[3]*/
    public static int[] collatz( int[] data ) {
        if (data[0]<=0) throw new IllegalArgumentException( "The collatz function can only take a positive integer as input parameter");
        if (data[0]==1) return data;
        else {
            data[1]=data[1]+1;
            data[0]=(data[0]%2==0) ? (data[0]/2) : ((data[0]*3)+1) ;
            data[2]=Integer.max(data[2], data[0]);
            return collatz(data);
        }
    }

    public static void main(String[] args) {
        int[] data = {0,0,0};
        Collatz.collatz(data);
        System.out.println(data[0] + " " + data[1] + " " + data[2]);
    }
}
