package lu.ausy.java;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import java.util.stream.Stream;

public class CollatzTest {

    @Test
    public void testCollatzWithNegativeInteger(){
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> Collatz.collatz(new int[]{-1,0,0}));
        Assert.assertEquals("The collatz function can only take a positive integer as input parameter", exception.getMessage());
    }

    @Test
    public void testCollatzWithZero(){
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> Collatz.collatz(new int[]{0,0,0}));
        Assert.assertEquals("The collatz function can only take a positive integer as input parameter", exception.getMessage());
    }

    private static Stream<Arguments> provideCollatzContexts() {
        return Stream.of(
                Arguments.of(10,6,16),
                Arguments.of(1003,41,8584),
                Arguments.of(850325,113,2550976),
                Arguments.of(35632562,214,60129952)
        );
    }

    @ParameterizedTest
    @MethodSource("provideCollatzContexts")
    public void testCollatz( int number, int expectedRecursion, int expectedMaxNumber){
        int[] context = {number,0,0};
        Collatz.collatz(context);

        Assert.assertEquals(1,context[0]);
        Assert.assertEquals(expectedRecursion, context[1]);
        Assert.assertEquals(expectedMaxNumber, context[2]);
    }
}
